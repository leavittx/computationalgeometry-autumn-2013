import fileinput
import time
import decimal

DEBUG           = False
CHECK_CONVEXITY = False
BENCHMARK       = False
CHECK_SLOW      = False
VERBOSE_RESULT  = False
ACCEPT_DECIMAL  = False

class Point():
    """
    Point/vector class
    """
    def __init__(self, x=0, y=0):
        # We're working only with integer coordinates
        self._x = int(x)
        self._y = int(y)
    @property
    def x(self):
        return self._x
    @x.setter
    def x(self, value):
        self._x = value
    @property
    def y(self):
        return self._y
    @y.setter
    def y(self, value):
        self._y = value
    def __pos__(self):
        return Point(self._x, self._y)
    def __neg__(self):
        return Point(-self._x, -self._y)
    def __add__(self, other):
        return Point(self._x + other.x, self._y + other.y)
    def __sub__(self, other):
        return Point(self._x - other.x, self._y - other.y)
    def __eq__(self, other):
        return self._x == other.x and self._y == other.y
    def __repr__(self):
        return "({}, {})".format(self._x, self._y)
    def rot90ccw(self):
        """ Rotate by 90 degrees counter-clockwise """
        return Point(-self._y, self._x)
    def rot90cw(self):
        """ Rotate by 90 degrees clockwise """
        return Point(self._y, -self._x)
    def rot180ccw(self):
        """ Rotate by 180 degrees counter-clockwise """
        return Point(-self._x, -self._y)
    def length(self):
        """ Length """
        return (self.x * self.x + self._y * self._y) ** 0.5
    def length2(self):
        """ Squared length """
        return self.x * self.x + self._y * self._y
    def projection_length(self, u):
        """ Calculates length of projection of self onto u """
        return dot(self, u) / u.length()
    def projection_length_unnormalized(self, u):
        """ Calculates length of projection of self onto u * length(u)
            Note: actually it's simply the dot product of two vectors! """
        return dot(self, u)

def dot(u, v):
    """ Gives dot product of u and v """
    return u.x * v.x + u.y * v.y

def up(u, v):
    """ True only if v is directed up in direction u """
    return dot(u, v) > 0

def down(u, v):
    """ True only if v is directed down in direction u """
    return dot(u, v) < 0

def direction_sign(u, v_i, v_j):
    """ Direction sign of v_i - v_j """
    return dot(u, v_i - v_j)

def above(u, v_i, v_j):
    """ True only if v_i is above v_j in direction u """
    return direction_sign(u, v_i, v_j) > 0

def below(u, v_i, v_j):
    """ True only if v_i is below v_j in direction u """
    return direction_sign(u, v_i, v_j) < 0


def signed_area(p0, p1, p2):
    """ Calculates 2*(signed area) of triangle defined by points p0, p1, p2 """
    s = (p1.x - p0.x) * (p2.y - p0.y) - (p2.x - p0.x) * (p1.y - p0.y)
    #print("Checking", p0, p1, p2, "2*area = ", s)
    return s


class Polygon():
    """
    Simple polygon class
    """
    TRAVERSAL_ORDER_CCW = "CCW"
    TRAVERSAL_ORDER_CW  = "CW"
    TRAVERSAL_ORDER_NA  = "NA"

    def __init__(self, vertices=None):
        # at least triangle expected here
        if vertices is not None:
            assert len(vertices) >= 3
        self._shell = vertices
        # make polygon shell a closed linear ring
        if self.get_vertex(0) != self.get_vertex(-1):
            self._shell.append(self.get_vertex(0))

    def empty(self):
        del self._shell
        self._shell = None

    def __del__(self):
        self.empty()

    #def gtag(self):
    #    return hash(repr(self))

    def __len__(self):
        """
        Returns number of vertices in polygon shell
        """
        return len(self._shell)

    @property
    def length(self):
        """
        Returns number of vertices in polygon, which equal to the number of edges
        """
        return self.__len__() - 1

    def __iter__(self):
        self._index = 0
        self._length = self.__len__()
        return self

    def __next__(self):
        if self._index < self._length:
            ring = self.get_vertex(self._index)
            self._index += 1
            return ring
        else:
            raise StopIteration

    def get_vertex(self, idx):
        """
        Supports negative indices
        """
        assert abs(idx) < self.__len__()
        if idx >= 0:
            return self._shell[idx]
        else:
            return self._shell[self.__len__() - abs(idx)]

    def __repr__(self):
        s = []
        for v in self:
            s.append(str(v) + ' ')
        return ''.join(s)

    def check_convexity(self):
        """
        Complexity: O(N)
        """
        assert self._shell is not None

        traversal_order = self.TRAVERSAL_ORDER_NA
        for idx in range(0, len(self._shell) - 2):
            s = signed_area(*self._shell[idx:idx + 3])
            if s < 0:
                if traversal_order is self.TRAVERSAL_ORDER_CCW:
                    return False, self.TRAVERSAL_ORDER_NA
                traversal_order = self.TRAVERSAL_ORDER_CW
            elif s > 0:
                if traversal_order is self.TRAVERSAL_ORDER_CW:
                    return False, self.TRAVERSAL_ORDER_NA
                traversal_order = self.TRAVERSAL_ORDER_CCW
            else:
                # Polygon goes straight...
                pass

        return True, traversal_order

    def get_edge(self, idx):
        """
        Get edge vector
        """
        # TODO: check bounds
        return self.get_vertex(idx + 1) - self.get_vertex(idx)

    def find_extreme_max_point_index(self, u):
        """
        Use the property of that convex polygon is monotonic along any arbitrary direction u
        So binary search is applicable!
        Complexity: O(log N)
        """
        assert self._shell is not None

        a = 0
        b = self.__len__()
        edge_a = self.get_edge(0)
        up_edge_a = up(u, edge_a)

        if not up_edge_a and not above(u, self.get_vertex(-2), self.get_vertex(0)):
            return 0

        while 1:
            c         = int((a + b) / 2)
            edge_c    = self.get_edge(c)
            up_edge_c = up(u, edge_c)

            if not up_edge_c and not above(u, self.get_vertex(c - 1), self.get_vertex(c)):
                return c

            if up_edge_a:
                if not up_edge_c:
                    b = c # search on [a, c]
                else:
                    if above(u, self.get_vertex(c), self.get_vertex(a)):
                        a = c # search on [c, b]
                        up_edge_a = up_edge_c
                    else:
                        b = c # search on [a, c]
            else:
                if up_edge_c:
                    a = c # search on [c, b]
                    up_edge_a = up_edge_c
                else:
                    if below(u, self.get_vertex(c), self.get_vertex(a)):
                        a = c # search on [c, b]
                        up_edge_a = up_edge_c
                    else:
                        b = c # search on [a, c]

            # Should not happen ever
            if b <= a + 1:
                raise IndexError

    def find_extreme_min_point_index(self, u):
        """
        Complexity: O(log N)
        """
        return self.find_extreme_max_point_index(-u)

    def __bounding_rectangle_compute_measures_float(self, configuration, direction):
        """
        Encapsulation note: this method can be accessed outside Polygon class via
        polygon = Polygon()
        polygon._Polygon__bounding_rectangle_compute_measures_float()
        """
        u, u_orthogonal = direction, direction.rot90ccw()
        projection_a = self.get_vertex(configuration[0]).projection_length(u_orthogonal)
        projection_b = self.get_vertex(configuration[1]).projection_length(u)
        projection_c = self.get_vertex(configuration[2]).projection_length(u_orthogonal)
        projection_d = self.get_vertex(configuration[3]).projection_length(u)
        dim1 = abs(projection_c - projection_a)
        dim2 = abs(projection_d - projection_b)
        area = dim1 * dim2
        perimeter = (dim1 + dim2) * 2
        return area, perimeter

    def __bounding_rectangle_compute_measures_integer(self, configuration, direction):
        u, u_orthogonal = direction, direction.rot90ccw()
        a = self.get_vertex(configuration[0])
        b = self.get_vertex(configuration[1])
        c = self.get_vertex(configuration[2])
        d = self.get_vertex(configuration[3])
        w = abs((c - a).projection_length_unnormalized(u_orthogonal))
        h = abs((d - b).projection_length_unnormalized(u))
        area_numerator = w * h
        perimeter_numerator   = (w + h) ** 2
        return area_numerator, perimeter_numerator

    def min_bounding_rectangle_simple(self):
        """
        Complexity: O(N log N)
        """
        # initial configuration
        u = self.get_edge(0)
        u_orthogonal = u.rot90ccw()
        a = 0
        b = self.find_extreme_max_point_index(u)                            # O(log N)
        c = self.find_extreme_max_point_index(u_orthogonal)                 # O(log N)
        d = self.find_extreme_min_point_index(u)                            # O(log N)
        cfgs = (a, b, c, d)
        # initial configuration is optimal for now
        min_area, min_perimeter = self.__bounding_rectangle_compute_measures_integer(cfgs, u)
        min_area_configuration = min_perimeter_configuration = cfgs
        min_area_dir, min_perimeter_dir = u, u

        # loop over configurations
        for idx in range(1, self.length):                                       # O(N)
            u = self.get_edge(idx)
            assert isinstance(u, Point)
            u_orthogonal = u.rot90ccw()
            a = idx
            b = self.find_extreme_max_point_index(u)                            # O(log N)
            c = self.find_extreme_max_point_index(u_orthogonal)                 # O(log N)
            d = self.find_extreme_min_point_index(u)                            # O(log N)

            cfgc = (a, b, c, d)
            area, perimeter = self.__bounding_rectangle_compute_measures_integer(cfgc, u)

            if area * min_area_dir.length2() < min_area * u.length2():
                min_area = area
                min_area_configuration = cfgc
                min_area_dir = u
            if perimeter * min_perimeter_dir.length2() < min_perimeter * u.length2():
                min_perimeter = perimeter
                min_perimeter_configuration = cfgc
                min_perimeter_dir = u

        return min_area_configuration, min_perimeter_configuration,\
               min_area / min_area_dir.length2(), 2 * min_perimeter ** 0.5 / min_perimeter_dir.length()


    def min_bounding_rectangle_calipers(self):
        """
        Complexity: O(N)
        """
        def min_angle(a, b, c, d):
            """
            Finds among a, b, c, d edges one that requires caliper rotation of minimal angle
            Returns index of param 0..3 which corresponds to such edge
            """
            edge_a = self.get_edge(a)
            edge_b = self.get_edge(b).rot90cw()
            edge_c = self.get_edge(c).rot180ccw()
            edge_d = self.get_edge(d).rot90ccw()

            sa = signed_area(Point(0, 0), edge_a, edge_b)
            if sa > 0:
                min_angle_edge = edge_a
                min_index      = 0
            else:
                min_angle_edge = edge_b
                min_index      = 1

            sa = signed_area(Point(0, 0), min_angle_edge, edge_c)
            if sa < 0:
                min_angle_edge = edge_c
                min_index      = 2

            sa = signed_area(Point(0, 0), min_angle_edge, edge_d)
            if sa < 0:
                min_angle_edge = edge_d
                min_index      = 3

            return min_index

        def next_configuration(configuration):
            cur_idx = (configuration[0] + 1) % self.length
            min_angle_param_number = min_angle(cur_idx, configuration[1], configuration[2], configuration[3])
            if min_angle_param_number   == 0:
                return [cur_idx, configuration[1], configuration[2], configuration[3]]
            elif min_angle_param_number == 1:
                return [configuration[1], configuration[2], configuration[3], cur_idx]
            elif min_angle_param_number == 2:
                return [configuration[2], configuration[3], cur_idx, configuration[1]]
            elif min_angle_param_number == 3:
                return [configuration[3], cur_idx, configuration[1], configuration[2]]

        # first, find extreme points along horizontal/vertical directions
        u = Point(1, 0)
        u_orthogonal = u.rot90ccw()
        a = self.find_extreme_min_point_index(u_orthogonal)
        b = self.find_extreme_max_point_index(u)
        c = self.find_extreme_max_point_index(u_orthogonal)
        d = self.find_extreme_min_point_index(u)
        cfgs = [a, b, c, d]
        # second, determine the point where rotation of the caliper will be minimal
        min_angle_param_number = min_angle(*cfgs)
        # update the caliper direction and orthogonal to it
        u = self.get_edge(cfgs[min_angle_param_number])
        u_orthogonal = u.rot90ccw()
        # update the configuration
        a = cfgs[min_angle_param_number]
        b = self.find_extreme_max_point_index(u)
        c = self.find_extreme_max_point_index(u_orthogonal)
        d = self.find_extreme_min_point_index(u)
        # remember initial configuration
        # 1st element in configuration is supporting edge index
        # 2nd, 3rd, 4th elements are extreme point indices
        cfgs = [a, b, c, d]

        # initial configuration is optimal for now
        min_area, min_perimeter = self.__bounding_rectangle_compute_measures_integer(cfgs, u)
        min_area_configuration = min_perimeter_configuration = cfgs
        min_area_dir, min_perimeter_dir = u, u

        # assign current configuration and compute current direction
        cfgc = next_configuration(cfgs)
        u = self.get_edge(cfgc[0])

        # loop over configurations
        while 1:
            # calculate area/perimeter measures for current bounding rectangle
            area, perimeter = self.__bounding_rectangle_compute_measures_integer(cfgc, u)

            # update minimal rectangle(s) if needed
            if area * min_area_dir.length2() < min_area * u.length2():
                min_area = area
                min_area_configuration = cfgc
                min_area_dir = u
            if perimeter * min_perimeter_dir.length2() < min_perimeter * u.length2():
                min_perimeter = perimeter
                min_perimeter_configuration = cfgc
                min_perimeter_dir = u

            # determine next configuration and update direction
            cfgc = next_configuration(cfgc)
            u = self.get_edge(cfgc[0])

            # loop end condition: we only need to check that supporting edge repeated again
            if cfgc[0] == cfgs[0]: #and cfgc[1] == cfgs[1] and cfgc[2] == cfgs[2] and cfgc[3] == cfgs[3]:
                break

        return min_area_configuration, min_perimeter_configuration,\
               min_area / min_area_dir.length2(), 2 * min_perimeter ** 0.5 / min_perimeter_dir.length()


def replace_characters(string, chars, value):
    """ Replace all of occurrences of any character from chars in string with value """
    translation_table = dict.fromkeys(map(ord, chars), value)
    return string.translate(translation_table)

def form_answer_verbose(cfg, polygon):
    assert isinstance(polygon, Polygon)
    edge_index1 = cfg[0]
    edge_index2 = (edge_index1 + 1) % polygon.length
    vert_index1, vert_index2, vert_index3 = cfg[1], cfg[2], cfg[3]
    return "Edge: ({}, {}) Extreme vertices: ({}, {}, {})".\
        format(edge_index1, edge_index2, vert_index1, vert_index2, vert_index3)

def form_answer(cfg, polygon):
    assert isinstance(polygon, Polygon)
    edge_index1 = cfg[0]
    edge_index2 = (edge_index1 + 1) % polygon.length
    vert_index1, vert_index2, vert_index3 = cfg[1], cfg[2], cfg[3]
    return "({}, {}), ({}, {}, {})".\
        format(edge_index1, edge_index2, vert_index1, vert_index2, vert_index3)


def main():
    coordinates = []
    vertices = []

    # read input data line by line
    if ACCEPT_DECIMAL:
        decimal_places_max = 0
    for line in fileinput.input():
        line = replace_characters(line, '{,}', ' ')
        for coord in line.split():
            if ACCEPT_DECIMAL:
                d = decimal.Decimal(coord)
                decimal_places = abs(d.as_tuple().exponent)
                decimal_places_max = max(decimal_places, decimal_places_max)
                coordinates.append(d)
            else:
                coordinates.append(coord)

    # transform decimal into int
    if ACCEPT_DECIMAL:
        coordinates = [int(d * (10 ** decimal_places_max)) for d in coordinates]

    # arrange into vertices of a polygon
    for x, y in zip(coordinates[0::2], coordinates[1::2]):
        vertices.append(Point(x, y))

    # create a polygon from vertices
    polygon = Polygon(vertices)

    if DEBUG:
        print("Iteration over the polygon:")
        for v in polygon:
            print(v, end=" ")
        print()
    if DEBUG:
        for idx in [-2, -1, 0, 1, 2]:
            print("Polygon vertex at position {}: {}".format(idx, polygon.get_vertex(idx)))
    if DEBUG:
        print("Polygon:", polygon)

    if CHECK_CONVEXITY:
        is_convex, traversal_order = polygon.check_convexity()
        if not is_convex or traversal_order is not Polygon.TRAVERSAL_ORDER_CCW:
            print("Convex: {}\nTraversal order: {}".format(is_convex, traversal_order))
            print("Invalid polygon has been given! Exiting now")
            return

    if DEBUG:
        dir = Point(1, 0)
        print("Extreme point in direction {}: {}".format(dir, polygon.get_vertex(polygon.find_extreme_max_point_index(dir))))

    if BENCHMARK:
        max_runs = 100
    else:
        max_runs = 1

    if CHECK_SLOW:
        start = time.time()
        for x in range(0, max_runs):
            minbr_area, minbr_perimeter, min_area, min_perimeter = \
                polygon.min_bounding_rectangle_simple()
        end = time.time()
        time_simple = end - start
        print("Min area bounding rect simple:        {} Area:      {}".format(form_answer_verbose(minbr_area, polygon), min_area))
        print("Min perimeter bounding rect simple:   {} Perimeter: {}".format(form_answer_verbose(minbr_perimeter, polygon), min_perimeter))

    start = time.time()
    for x in range(0, max_runs):
        minbr_area_calipers, minbr_perimeter_calipers, min_area_calipers, min_perimeter_calipers = \
            polygon.min_bounding_rectangle_calipers()
    end = time.time()
    time_calipers = end - start
    if VERBOSE_RESULT:
        print("Min area bounding rect calipers:      {} Area:      {}".format(form_answer_verbose(minbr_area_calipers, polygon), min_area_calipers))
        print("Min perimeter bounding rect calipers: {} Perimeter: {}".format(form_answer_verbose(minbr_perimeter_calipers, polygon), min_perimeter_calipers))
    else:
        print("Area:      {}\nPerimeter: {}".format(form_answer(minbr_area_calipers, polygon), form_answer(minbr_perimeter_calipers, polygon)))

    if CHECK_SLOW:
        # the results given by two different algorithms should be identical in sense of minimality of area/perimeter
        assert min_area_calipers == min_area and min_perimeter_calipers == min_perimeter

    if BENCHMARK:
        print("Time simple:   {}\nTime calipers: {}".format(time_simple, time_calipers))


if __name__ == "__main__":
    main()
